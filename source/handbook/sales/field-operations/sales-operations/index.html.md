---
layout: markdown_page
title: "Sales Operations"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Welcome to Sales Operations!

**HOW TO COMMUNICATE WITH US**

Slack: [#sales-support](https://gitlab.slack.com/messages/CNLBL40H4)  
SFDC: [@Sales-Ops](https://gitlab.my.salesforce.com/_ui/core/chatter/groups/GroupProfilePage?g=0F9610000001qPC)

**CHARTER**

Sales Operations is responsible for helping make sales more effective and efficient. We aim to help facilitate new and existing processes throughout our field organization via the use of systems, policies and direct support. Our internal goal is to automate and streamline as much as possible to allow us to get more done, but where there is no automation, we aim to provide direct, white-glove support to our sales organization.

Sales Operations is a part of (Field Operations), and is responsible for the following key areas:

*  Territories
*  Go To Market data
*  Deal Support (see Deal Desk)
*  Compensation
*  Partner Operations
*  Customer Success Operations



