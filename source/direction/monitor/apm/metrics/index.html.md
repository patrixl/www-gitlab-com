---
layout: markdown_page
title: "Category Vision - Metrics"
---

- TOC
{:toc}

## Metrics
Metrics help users understand how their applications are performing, and if they are healthy. Examples of common metrics include response metrics like latency and error rate, system metrics like cpu and memory consumption, as well as any other type of telemetry desired.

Actions and insights can then be derived from these metrics like setting Service Levels, Error Budgets, as well as triggering alerts. 

## Target audience and experience

Metrics are an important tool for all users across the DevOps spectrum. From pure developers who should understand the performance impact of changes they are making, as well as pure operators who are responsible for keeping production services online.

The target workflow includes a few important use cases:
1. Configuring GitLab to monitor an application should be as easy as possible. To the degree possible given the environment, we should automate this activity for our users.
1. Dashboards should automatically populate with relevant metrics that were detected, however still offer flexibility to be customized as needed for a specific use case or application. The dashboards themselves should offer the visualizations required to best represent the data.
1. When troubleshooting, we should offer the ability to easily explore the data to help understand potential relationships and create/share one off dashboards.
1. Alerts should be easy to create, and provide a variety of notification options include Issues and third party services like Slack. It would also be great if we could provide some automatic detection of outliers/anomalies, and out of the box alerts based on best practices.
1. Service Level Objectives should be able to be defined, with corresponding impact on Error Budgets when they are not met.

## What's Next & Why
Next up is the ability to support multiple queries per chart, which is an important building block to [improve our charting dashboard](https://gitlab.com/gitlab-org/gitlab-ce/issues/54877). 

This allows multiple time series to be represented in the same chart, that require different queries to generate. For example showing both the p95 and p99 buckets in the same chart.

## Maturity Plan
* [Base Epic](https://gitlab.com/groups/gitlab-org/-/epics/534)

## Competitive Landscape
[Datadog](https://www.datadoghq.com/) and [New Relic](https://newrelic.com/) are the top two competitors in this space.

## Analyst Landscape
Gartner does a review of APM products in their [APM Magic Quadrant](https://www.gartner.com/doc/3868870). 

We are working to set up meetings with analysts to gather their feedback.

## Top Customer Success/Sales Issue(s)
Not yet, but accepting merge requests to this document.

## Top Customer Issue(s)
[Support for NGINX Ingress 21+](https://gitlab.com/gitlab-org/gitlab-ce/issues/57746)

## Top Internal Customer Issue(s)
Not yet, but accepting merge requests to this document.

## Top Vision Item(s)
Not yet, but accepting merge requests to this document.
